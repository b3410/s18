 

function Pokemon(name, lvl, hp, atk){
	this.name = name;
	this.level = lvl;
	this.health = hp;
	this.attack = atk;

	// methods
	this.tackle = function(target){
		console.log(this.name + ' '+ 'tackled ' + target.name)
	};

	this.hpRem = function(target){
		this.health = this.health - target.attack;
		console.log(this.name + ' remaining health points is ' + this.health )
	};

	this.faint = function(){

		if (this.health < 10){console.log(this.name + ' ' + 'fainted');
		}
	
		else {console.log(this.name+ ' won the battle!' )}

	}
}

	//this.health = this.health - target.attack


// creating instance

let bulbasaur = new Pokemon('Bulbasaur', 5, 30, 8);
let charizard = new Pokemon('Charizard', 10, 40, 13);

charizard.tackle(bulbasaur);
bulbasaur.hpRem(charizard);

bulbasaur.tackle(charizard);
charizard.hpRem(bulbasaur);

charizard.tackle(bulbasaur);
bulbasaur.hpRem(charizard);


bulbasaur.faint()
charizard.faint();


